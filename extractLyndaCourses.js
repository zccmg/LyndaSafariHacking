var webPage = require('webpage');
var Async =  require("async");
var fs = require("fs");
var _ =  require("lodash");
var page = webPage.create();



page.settings.loadImages = false;
page.onConsoleMessage = function(msg) {
  console.log("CONSOLE: ", msg);
}

page.open("https://www.lynda.com/Business-Skills-tutorials/Teamwork-Fundamentals/365728-2.html", function (status) {
  console.log("course accessed", status);
  Async.waterfall([
    function (finish) {
      var response = page.evaluate(function () {
        var topicGroupsElm = document.querySelectorAll('ol#course-toc-outer *[id^="toc-chapter"]');
        console.log(topicGroupsElm.length);
        var data = [];
        var idCourseCounter = 0;
        var idGroupCounter = 0;
        for(var i=0; i< topicGroupsElm.length; i++) {
          var groupElm = topicGroupsElm[i];
          var dat = {};
          var coursesElm = groupElm.querySelectorAll('*[id^="course-toc-inner"] li') || [];
          console.log("reached 5-", coursesElm.length);
          dat.id = idGroupCounter++;
          dat.groupTitle = groupElm.querySelector('*[id^="chapter-title"]').textContent;
          dat.courses = [];
          for(var j=0; j < coursesElm.length; j++) {
            var courseElm = coursesElm[j];
            var a = courseElm.querySelector('*[id^="toc-video"] dt a');
            console.log("reached 6-", a.textContent);
            if (a) {
              dat.courses.push({
                url: a.getAttribute("href"),
                name: a.textContent,
                id: idCourseCounter++
              })
            }
          }
          console.log("reached 7-", idCourseCounter, JSON.stringify(dat));
          data.push(dat);
        }

        return data;
      });
      finish(null, response);
    }
  ], function (err, result) {
    if(err) {
      console.log(err);
      phantom.exit();
    } else {
      try {
        fs.write("responseLynda.json", JSON.stringify(result), "w");
        phantom.exit();
      } catch (ex) {
        console.log(ex);
      }
    }
  })

})
