var Async =  require("async");
var fs = require("fs");
var mkdirp = require("mkdirp");
var PDFMerge = require('pdf-merge');
var Path = require("path");
var _ =  require("lodash");
var pdf = require('html-pdf');
var bookDir = "H:/Books/";
var data = JSON.parse(fs.readFileSync(__dirname + "/completedBook.json").toString());
var result = [];

Async.eachOf(data.pages, function (page, key, next) {
    var html = fs.readFileSync(page.url, 'utf8');
    pdf.create(html, {
        height: (page.height / 96) + "in",
        width: "6.25in",
        border: "10px",
        timeout: 9000000,
        base: "https://www.safaribooksonline.com"
    }).toFile(page.url.replace(".html", ".pdf"), function(err, res) {
        result.push(res.filename);
        if (err) return next(err);
        next();
    });

}, function (err) {
    console.log(err);
    var pdfMerge = new PDFMerge(result, Path.join(__dirname, "pdftk/pdftk.exe"));
    pdfMerge.asNewFile(Path.join(bookDir, data.title + ".pdf")).merge(function(error, result) {
        console.log(error, result);
        process.exit(0);
    });
});






