
var mkdirp = require("mkdirp");
var Fs = require("fs");
var URI = require("uri-js");
var Async = require("async");
var Path = require("path");
var _ = require("lodash");

var data = JSON.parse(Fs.readFileSync(__dirname + "/completeLynda.json").toString());
var parentPath = "H:/Teamwork Fundamentals";


Async.each(data, function (group, done) {
    var groupPath = Path.join(parentPath, group.id + ". " + group.groupTitle.replace(/[\"\?:\\\/]+/g, ""));

    Async.series([
        Async.apply(mkdirp, groupPath),
        function (next) {
            Async.eachLimit(group.courses, 1, function (course, finish) {
                var task = {
                    path: Path.join(groupPath, course.id + ". " + course.name.replace(/[\"\?:\\\/]+/g, " ") + Path.extname(course.video.split("?")[0])),
                    url: URI.serialize(URI.parse(course.video))
                };

                if(Fs.existsSync(task.path) && !Fs.existsSync(task.path + ".aria2")) {
                    return finish();
                }
                var exec = require('child_process').execFile;
                var appPath = Path.join(__dirname, "aria2/aria2c.exe");
                var call = exec(appPath, [
                    "-x16",
                    "-l logs.txt",
                    "-d " + Path.dirname(task.path),
                    "-o " + Path.basename(task.path),
                    task.url
                ], function(error, stdout, stderr) {
                    if (error !== null) {
                        console.log('exec error: ', error);
                    }
                    finish();
                });
                call.stdout.on('data', function(chunk) {
                    console.log("INFO:", chunk);
                });
                call.stderr.on('error', function(chunk) {
                    console.log("ERROR:", chunk);
                });
            }, next)
        }
    ], done)
}, function (err) {
    console.log("All data added to the queue");
});
